use std::sync::Arc;
use std::path::PathBuf;
use std::path::Path;
use std::fs::File;
use std::io::{BufRead, BufReader};
use warp::path::FullPath;
use warp::{Filter, http::Response};
use anyhow::{Result, format_err};
use pulldown_cmark::{Parser, Options, html};
use std::fs::read_to_string;
use log;
use structopt::StructOpt;
use std::ffi::OsString;
use std::collections::HashMap;

const HTML_TEMPLATE: &'static str = include_str!("../resources/main_template.html");

const STYLE_SHEET_TEMPLATE: &'static str = r#"<link rel="stylesheet" href="$CSS_URL$">"#;

const DEFAULT_STYLE_SHEET: &'static str = include_str!("../resources/style.css");

const MAX_SEARCH_RESULTS: usize = 20;

#[derive(Debug, StructOpt)]
#[structopt(name = "md server", about = "a simple server that serves md files")]
struct ServerOpts {
    #[structopt(name = "BASE-PATH", parse(from_os_str), default_value = ".")]
    base_path: PathBuf,

    #[structopt(short, long)]
    css_uri: Option<String>,

    #[structopt(short, long, default_value = "3012")]
    port: u16,
}

fn exists_and_is_file(path: &Path) -> bool {
    if !path.exists() {
        return false;
    }

    match std::fs::metadata(path) {
        Ok(metadata) => metadata.is_file(),
        Err(e) => {
            log::error!("could not determine file type of {:?}", e);
            false
        }
    }
    
}

fn find_file<P: AsRef<Path>>(path: P) -> Option<PathBuf> {
    let mut result = PathBuf::new();
    result.push(path);

    log::debug!("checking for {:?}", &result);
    if exists_and_is_file(&result) {
        log::debug!("exists");
        return Some(result);
    }

    if let Some(file_name) = result.file_name() {
        let mut file_name = OsString::from(file_name);
        file_name.push(".md");
        let mut new_result = result.clone();
        new_result.pop();
        new_result.push(file_name);
        log::debug!("checking for {:?}", &new_result);
        if exists_and_is_file(&new_result) {
            log::debug!("exists");
            return Some(new_result);
        }
    }

    result.push("index.md");

    log::debug!("falling back to {:?}", &result);

    if exists_and_is_file(&result) {
        return Some(result);
    } 

    return None;
}

fn read_parse_convert<P: AsRef<Path>>(path: P) -> Result<String> {
    let path = find_file(path).ok_or(format_err!("unable to access file"))?;
    let md_content = read_to_string(&path)?;
    let parser = Parser::new_ext(&md_content, Options::all());
    let mut html_output = String::new();
    html::push_html(&mut html_output, parser);
    Ok(html_output)
}

fn handle_request(server_opts: &ServerOpts, path: &FullPath) -> Result<String> {
    let mut serve_path = server_opts.base_path.clone();
    let mut path = &path.as_str()["/pages".len()..];
    if path.starts_with("/") {
        path = &path[1..];
    }
    serve_path.push(&path);

    log::debug!("accessing {:?}", &serve_path);

    match read_parse_convert(&serve_path) {
        Ok(body) => {
            log::info!("accessing {:?} ok", &serve_path);
            Ok(body)
        },
        Err(e) => {
            log::error!("error accessing {:?}: {:?}", &serve_path, &e);
            Err(format_err!("unable to access {:?}", &path))
        }
    }.map(|body| {
        let style = match &server_opts.css_uri {
            Some(url) => STYLE_SHEET_TEMPLATE.replace("$CSS_URL$", url),
            None =>  STYLE_SHEET_TEMPLATE.replace("$CSS_URL$", "/style.css")
        };

        HTML_TEMPLATE
            .replace("$TITLE$", &path)
            .replace("$STYLE$", &style)
            .replace("$BODY$", &body)
    })
}

struct SearchResult {
    pub path: String,
    pub matched_line: String,
    pub line_no: Option<u64>,
}

fn handle_search_for(server_opts: &ServerOpts, search_query: &str) -> Result<String> {
    log::info!("searching for {:?}", &search_query);
    let base_path = server_opts.base_path.to_str().unwrap();
    let glob_expression = format!("{}/**/*.md", base_path);
    log::debug!("glob_expression {:?}", glob_expression);
    let mut matches: Vec<SearchResult> = Vec::new();
    'search: for entry in glob::glob(&glob_expression)? {
        log::debug!("checking for {:?}", entry);
        if let Ok(path) = entry {
            let file = File::open(&path)?;
            let buf_reader = BufReader::new(file);
            for (line_no, line) in buf_reader.lines().enumerate() {
                if let Ok(line) = line {
                    if line.contains(&search_query) {
                        matches.push(SearchResult{
                            path: path.to_str().map(ToString::to_string).unwrap_or_else(|| String::new()),
                            matched_line: line.to_string(),
                            line_no: Some(line_no as u64)
                        });
                        if matches.len() >= MAX_SEARCH_RESULTS {
                            break 'search;
                        }
                    }
                }
            }
        }
    }

    let mut body = String::new();
    body += &format!("<h1>Search results for \"{}\"</h1>\n", &search_query);
    body += "<ul>\n";

    for entry in &matches {
        let line_no = entry.line_no.unwrap_or(0);
        body += &format!("  <li><a href=\"/pages/{path}\"><b>{path}</b></a>:{line_no} <pre>{capture}</pre></li>\n", 
            path = &entry.path, 
            line_no = line_no,
            capture = &entry.matched_line
        );
    }
    
    body += "</ul>\n";

    let style = match &server_opts.css_uri {
        Some(url) => STYLE_SHEET_TEMPLATE.replace("$CSS_URL$", url),
        None =>  STYLE_SHEET_TEMPLATE.replace("$CSS_URL$", "/style.css")
    };

    let html = HTML_TEMPLATE
        .replace("$TITLE$", "Search results")
        .replace("$STYLE$", &style)
        .replace("$BODY$", &body);


    Ok(html)
}

async fn run(server_opts: ServerOpts) -> Result<()> {

    let server_opts: Arc<ServerOpts> = Arc::new(server_opts);

    let page_server_opts = server_opts.clone();

    let page_server = warp::path("pages")
    .and(warp::path::full())
    .map(move |fp: FullPath| {
        let html = match handle_request(&page_server_opts, &fp) {
            Ok(html_output) => html_output,
            Err(e) => format!("<b>Error:</b><br /><pre>{:?}</pre>", e)
        };

        warp::reply::html(html)
    });

    let css_server = warp::path("style.css")
    .map(|| {
        Response::builder()
        .header("content-type", "text/css")
        .body(DEFAULT_STYLE_SHEET)
    });

    let search_server_opts = server_opts.clone();

    let search_server = warp::path("search")
      .and(warp::query::query())
      .map(move |param: HashMap<String, String>| {
          if let Some(query) = param.get("q") {
            let search_result_page = match handle_search_for(&search_server_opts, query) {
                Ok(output) => output,
                Err(e) => format!("<b>Error:</b><br /><pre>{:?}</pre>", e)
            };
            warp::reply::html(search_result_page)
        } else {
            warp::reply::html("invliad query".to_string())
        }
      });

    let redirect = warp::path::end()
      .map(|| warp::redirect(warp::http::Uri::from_static("/pages/index.md")));

    let pages = 
        page_server
        .or(css_server)
        .or(search_server)
        .or(redirect);

    let my_opts = server_opts.clone();

    warp::serve(pages)
        .run(([0, 0, 0, 0], my_opts.port))
        .await;

    Ok(())
}

#[tokio::main]
async fn main() {
    env_logger::builder().filter_level(log::LevelFilter::Info).init();

    let server_opts = ServerOpts::from_args();

    if let Err(e) = run(server_opts).await {
        println!("error: {:?}", e);
    }
}
